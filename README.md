# Setup for testing Prometheus metrics and rules
Docker compose setup for running Prometheus and Alert manager for testing and developing alert rules

![](doc/arch.png "Architecture")

Users can either use the default `metrics-app` and/or use their own local or remote service(s) by running the script `setup-services.sh`.

Then there is all the necessary plumbing and alerts should appear in the docker-compose log.

![](doc/alert.png "Alert Example")


## Basic usage
The only thing you need to have on your machine before you start is Docker.

1. `git clone git@bitbucket.org:jan_skrabal/prometheus-local.git` 
2. `cd prometheus-local`
3. `docker-compose up`

This will start 4 application as shown on the picture above.

* Prometheus is exposed on [localhost:9090](http://localhost:9090/)
* Alert Manager is exposed on [localhost:9093](http://localhost:9093/)

### How to add your own service metrics?
1. Start the service on the same host where the prometheus-local runs
2. Run script `setup-services.sh`. The script will guide you how to use it.

Script will generate scraping target into the folder `targets` in the project which is mounted to the prometheus container and prometheus watches it.
This means you can add/remove/update services as you will and all should be reloaded run-time. Script requires 2 arguments 
* `service_name` - name of the service
* `service_port` - port on which the metrics are exposed

Then you can specify 2 more optional arguments
* `metrics_path` - Path to metrics endpoint. If not specified, it defaults to `/metrics`
* `external_host` - IP or hostname of host on which the service runs

More details are printed when you run `./setup-services.sh --help`.

You can use the same script for either adding new service or if you specify `service_name` of already existing one, its scraper will be updated.
All targets scraped by prometheus along with the state are available on [localhost:9090/targets](http://localhost:9090/targets).

If you wish to not have the `metrics-app` target then feel free to comment out in both the 
`docker-compose.yml` and `prometheus.config.yml` files.

Alerts and rules is all the same and described below.

### How to change metrics-app metric value or format?
Simple Metrics App just exposes metrics in [metrics file](metrics/metrics) so if you change it, next prometheus scrape will get the latest values.
The file is in the format expected by prometheus scraper so you can add metrics, remove metrics, change values, do whatever you want with it as long as it is valid.

Please be patient, it may take a while (~30 seconds) for alert to fully propagate.

### Where to put my alert definition(s)?
Update file [prometheus.rules.yml](prometheus.rules.yml). File has standard format as described in the [documentation](https://prometheus.io/docs/prometheus/latest/getting_started/#configure-rules-for-aggregating-scraped-data-into-new-time-series)
And in the [Template Reference](https://prometheus.io/docs/prometheus/latest/configuration/template_reference/) and [Template examples](https://prometheus.io/docs/prometheus/latest/configuration/template_examples/) you can find more about how to customise your alerts.

**Important:** Rules file is not hot-reloadable so you need to tell Prometheus to reload it. You can do it by sending following request

`curl -X POST http://localhost:9090/-/reload`

(If you have IntelliJ Idea, you can run it from file [useful-requests.http](useful-requests.http))

### How to test alert routing?
Alert manager uses routing mechanism to resolve where to publish the alert. Alert receiver can be a simple web-hook, Slack, Wechat, whatever.
Receivers and routing is defined in the [alertmanager.config.yml](alertmanager.config.yml) (file format is described in the [documentation](https://prometheus.io/docs/alerting/configuration/))
Wandera routing is defined in [wandera infrastructure oss chart](https://bitbucket.org/snappli-ondemand/kubernetes-descriptors/src/master/wandera-infrastructure/charts-oss.yaml)

### Handy Links
You need to run `docker-compose up` first before you can use the following links

* [Alert Status in Prometheus](http://localhost:9090/alerts)
* [Alert Status in Alert Manager](http://localhost:9093/#/alerts)
* [Metrics metrics-app endpoint scraped by Prometheus](http://localhost:8000)
* [Scraped targets and their current state](http://localhost:9090/targets)

### Contributors
* Jan Skrabal
* David Pryce